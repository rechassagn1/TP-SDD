#include <stdio.h>
#include <stdlib.h>
#include "abr.h"
#include "piles_files/data.h"
#include "piles_files/pile.h"
#include "piles_files/files.h"

/* Allocate new tree node and initialize it with `v` */
node_t * createNode(int v)
{
  node_t * newnode = (node_t*) malloc(sizeof(node_t));
  if (newnode) {
    newnode->fd = NULL;
    newnode->fg = NULL;
    newnode->valeur = v;
  }
  return newnode;
}

node_t **searchPrev(node_t * brt, int v)
{
  node_t ** prev = NULL;
  node_t * cur = brt;

  while (cur) {
    if (cur->valeur > v) {
      prev = &(cur->fg);
      cur = cur->fg;
    }
    else {
      prev = &(cur->fd);
      cur = cur->fd;
    }
  }
  return prev;
}

/* add a value v to the given tree */
node_t * addValue(node_t* brt, int v)
{
  node_t ** prev = searchPrev(brt, v);
  node_t * newnode = NULL;

  if (prev) {
    newnode = createNode(v);
    if (newnode)
      *prev = newnode;
  }
  else {
    brt = createNode(v);
  }
  return brt;
}

void freeAbr(node_t ** brt)
{
  pile_t * pile = initPile(100);
  node_t * cur = *brt;

  while (cur != NULL) {
    if (cur->fd)
      empiler(pile, (data_t) { .valeur = cur->fd });
    if (cur->fg)
      empiler(pile, (data_t) { .valeur = cur->fg });
    free(cur);
    cur = NULL; // free doesn't set cur to NULL

    if (!estVidePile(*pile)) {
      cur = depiler(pile).valeur;
    }
  }
  libererPile(&pile);
  *brt = NULL;
}

node_t ** searchFather(node_t ** brt, int v)
{
  node_t ** prev = brt;

  if (prev) {
    while (*prev && (*prev)->valeur != v) {
      if ((*prev)->valeur > v) {
	prev = &(*prev)->fg;
      }
      else if ((*prev)->valeur < v) {
	prev = &(*prev)->fd;
      }
    }
  }
  
  return prev;
}

node_t * deleteNode(node_t * brt, int v)
{
  node_t ** prev = searchFather(&brt, v);
  node_t * tmp;
  node_t * fg;
  node_t * fd;

  if (*prev) {
    tmp = *prev;
    fg = tmp->fg;
    fd = tmp->fd;
    *prev = fd;

    while (*prev) {
      prev = &(*prev)->fg;
    }
    *prev = fg;
    free(tmp);
  }
  return brt;
}
