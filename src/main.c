#include <stdio.h>
#include <stdlib.h>
#include "abr.h"
#include "piles_files/pile.h"

node_t * abrFromFile(char * fileName)
{
  FILE * f;
  int v = 0;
  node_t * racine = NULL;

  f = fopen(fileName, "r");
  if(f != NULL){
    while(!feof(f)){
      fscanf(f, "%d\n", &v);
      racine = addValue(racine, v);
    }
    fclose(f);
  }
  return racine;
}


void display(node_t * r){
  pile_t * p = initPile(100);
  int i = 0;
  node_t * cour = r;
  data_t depile;
  int fin = 0;
  
  if (p){
    while (!fin){
      while(cour != NULL){
	empiler(p, (data_t) {.valeur = cour, .i = i});
	cour = cour->fd;
	i++;
      }
      if(!estVidePile(*p)){
	depile = depiler(p);
	cour = depile.valeur;
	i = depile.i;
	for (int tab = 0; tab < i; tab++){
	  printf("\t");
	}
	printf("%d\n", cour->valeur);
	i ++;
	cour = cour->fg;
      }
      else fin = 1;
    }
    libererPile(&p);
  }
  else printf("Erreur allocation de la pile\n");
  
}


int height(node_t * r){
  pile_t * p = initPile(100);
  int i = 0;
  node_t * cour = r;
  data_t depile;
  int fin = 0;
  int max = 0;
  if (p){
    while (!fin){
      while(cour != NULL){
	empiler(p, (data_t) {.valeur = cour, .i = i});
	cour = cour->fd;
	i++;

      }
      if (i > max) max = i;

      if(!estVidePile(*p)){
	depile = depiler(p);
	cour = depile.valeur;
	i = depile.i;
	i ++;
	cour = cour->fg;
      }
      else fin = 1;
    }
    libererPile(&p);
  }
  else printf("Erreur allocation de la pile\n");
  return max;
}

int main(void)
{
  node_t * arbre = abrFromFile("./testfile.txt");
  display(arbre);
  printf("%d %d\n", arbre->valeur, arbre->fd->valeur);
  printf("height : %d\n", height(arbre));
  freeAbr(&arbre);
  return 0;
}



















































  /* node_t * brt = createNode(3); */
  /* brt = addValue(brt, 2); */
  /* brt = addValue(brt, 4); */
  /* printf("%d %d %d\n", brt->valeur, brt->fg->valeur, brt->fd->valeur); */
  /* freeAbr(&brt); */
